#include <iostream>
#include <math.h>
#include "Investment.h"
using namespace std;

int main()
{
	Investment Fire;
	int initInvestment, addInvestment, Amount, time;
	float interestRate;
	
	
	cout << "Enter the initial investment.\n";
	cin >> initInvestment;
	
	cout << "Enter the monthly investment.\n";
	cin >> addInvestment;
	
	cout << "Enter the interest rate.\n";
	cin >> interestRate;
	
	cout << "Enter the amount you want to save.\n";
	cin >> Amount;
	

	Fire.setInterestRate(interestRate);
	Fire.setValue(initInvestment);
	
	time = log((Amount - Fire.getValue())/addInvestment) / log(1+(Fire.getInterestRate()/12));
	
	cout << "It would take "<< time << " months to save $"<< Amount << '\n';
	cout <<"with an annual compound interest rate of "<< Fire.getInterestRate()*100 << "% \n";
	cout << "and monthly additional investments of $" << addInvestment<< ".\n";
	
	return 0;
}




