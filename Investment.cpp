#include "Investment.h"
using namespace std;

Investment::Investment(long double val, float int_rate)
{
	interest_rate = int_rate;
	value = val;
}

void Investment::setValue(long double val)
{
	value = val;
}
 
long double Investment::getValue()
{
	return value;
}

 void Investment::setInterestRate(float int_rate)
{
	interest_rate = int_rate;
}
 
 float Investment::getInterestRate()
{
	return interest_rate;
}
