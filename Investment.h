
class Investment
{
	private:  
	//properties
    long double value;
	float interest_rate; 
         


    public:  
    //methods  
	Investment(long double = 0.0L, float = 0.f);//default constructor 

	void setValue(long double val);
	long double getValue();

	void setInterestRate(float int_rate);
	float getInterestRate();
};
